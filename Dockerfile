FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY /src .

RUN dotnet publish "MyProject.csproj" -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:5.0

COPY --from=build /app/publish .

ENTRYPOINT ["dotnet", "MyProject.dll"]
